#!/usr/bin/env python3
import os
import sys
import gzip

dirname_data = os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                '..', 'data')

filename_gff3 = os.path.join(dirname_data,
                             'XENLA_9.2_Xenbase.2021_03_30.gff3.gz')
dirname_out = os.path.join(dirname_data, '..', 'gff3.by_chr')

f_gff3 = open(filename_gff3, 'r')
if filename_gff3.endswith('.gz'):
    f_gff3 = gzip.open(filename_gff3, 'rt')

headers = []
f_out_list = dict()
for line in f_gff3:
    if line.startswith('#'):
        headers.append(line.strip())
        continue

    tokens = line.strip().split("\t")
    seq_id = tokens[0]
    seq_type = tokens[2]

    if seq_id == 'MT':
        seq_id = 'chrM'
    elif not seq_id.startswith('chr'):
        seq_id = 'chrUn'

    if seq_id not in f_out_list:
        sys.stderr.write('seq_id: %s\n' % seq_id)
        filename_out = os.path.join(dirname_out, '%s.all.gff3' % seq_id)
        f_out_list[seq_id] = open(filename_out, 'w')
        f_out_list[seq_id].write('#gff-version 3\n')

    f_out_list[seq_id].write(line.strip()+"\n")
f_gff3.close()
